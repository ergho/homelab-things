resource "proxmox_vm_qemu" "vm" {
  for_each    = var.vm
  name        = each.value.name
  target_node = var.proxmox_host
  clone       = var.template_name
  agent       = 1
  os_type     = "cloud-init"
  cores       = 4
  sockets     = 1
  cpu         = "host"
  memory      = 2048
  scsihw      = "virtio-scsi-pci"
  bootdisk    = "scsi0"

  disk {
    slot     = 0
    size     = each.value.disk_size
    type     = "scsi"
    storage  = var.storage_name
    iothread = 1
  }

  network {
    model  = "virtio"
    bridge = "vmbr0"
  }

  sshkeys   = file("${var.ssh_key_file}")
  ipconfig0 = each.value.ipconfig

}
