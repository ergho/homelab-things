#!/usr/bin/env bash

set -o errexit

rm -f /var/lib/rpm/__db.*

exit 0
