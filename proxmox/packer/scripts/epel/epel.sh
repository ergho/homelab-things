#!/bin/bash -eux

# tries to remove subscription-manager, can cause issues in centos
# don't think its still in, but left here for good measure
dnf erase subscription-manager -y

# epel reposity 9 series
dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm -y

# epel next repository
#dnf install epel-next-release -y
