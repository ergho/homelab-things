variable "proxmox_host_node" {
  type = string
}

variable "proxmox_password" {
  type      = string
  sensitive = true
}

variable "proxmox_source_template" {
  type = string
}

variable "proxmox_template_name" {
  type = string
}

variable "proxmox_url" {
  type = string
}

variable "proxmox_username" {
  type    = string
  default = "root@pam"
}

source "proxmox-clone" "test-cloud-init" {
  insecure_skip_tls_verify = true
  full_clone               = false

  template_name = "${var.proxmox_template_name}"
  clone_vm      = "${var.proxmox_source_template}"

  os              = "l26"
  cores           = "1"
  memory          = "2048"
  scsi_controller = "virtio-scsi-pci"

  ssh_username = "rocky"
  qemu_agent   = true

  node        = "${var.proxmox_host_node}"
  username    = "${var.proxmox_username}"
  password    = "${var.proxmox_password}"
  proxmox_url = "${var.proxmox_url}"
}

//build {
//  sources = ["source.proxmox-clone.test-cloud-init"]
//  name = "podman"
//  provisioner "shell" {
//      execute_command = "sudo -E bash '{{ .Path }}'"
//      scripts = [
//      "./scripts/common/update.sh",
//      "./scripts/epel/epel.sh",
//      "./scripts/podman/podman.sh",
//      "./scripts/common/sysprep-rpm-db.sh",
//      "./scripts/common/sysprep-bash-history.sh",
//      ]
//  }
//  provisioner "shell" {
//    inline = ["sudo cloud-init clean"]
//  }
//}

build {
  sources = ["source.proxmox-clone.test-cloud-init"]
  name = "kubernetes"
  provisioner "shell" {
      execute_command = "sudo -E bash '{{ .Path }}'"
      scripts = [
      "./scripts/common/update.sh",
      "./scripts/common/randomthings.sh",
      "./scripts/kubernetes/criok8.sh",
      "./scripts/common/sysprep-rpm-db.sh",
      "./scripts/common/sysprep-bash-history.sh",
      ]
  }
  provisioner "shell" {
    inline = ["sudo cloud-init clean"]
  }
}
