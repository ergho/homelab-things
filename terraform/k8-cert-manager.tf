resource "kubernetes_namespace" "cert-manager-ns" {
  metadata {
    labels = {
      app = "cert-manager"
    }
    name = var.cert-manager_namespace
  }
}


resource "helm_release" "cert-manager" {
  name       = "cert-manager"
  repository = var.cert-manager_repository
  chart      = "cert-manager"
  namespace  = var.cert-manager_namespace
  version    = var.cert-manager_version
  values = [
    "${file("cert-manager-config/config.yaml")}"
  ]

  depends_on = [
    kubectl_manifest.calico,
    kubernetes_namespace.cert-manager-ns,

  ]
}

data "kubectl_file_documents" "letsencrypt-staging" {
  content = file("cert-manager-config/letsencrypt-staging.yaml")
}

data "kubectl_file_documents" "letsencrypt-prod" {
  content = file("cert-manager-config/letsencrypt-prod.yaml")
}

resource "kubectl_manifest" "letsencrypt-staging" {
  count     = length(data.kubectl_file_documents.letsencrypt-staging.documents)
  yaml_body = element(data.kubectl_file_documents.letsencrypt-staging.documents, count.index)
  depends_on = [
    helm_release.cert-manager,
    kubectl_manifest.istio
  ]
}

resource "kubectl_manifest" "letsencrypt-prod" {
  count     = length(data.kubectl_file_documents.letsencrypt-prod.documents)
  yaml_body = element(data.kubectl_file_documents.letsencrypt-prod.documents, count.index)
  depends_on = [
    helm_release.cert-manager,
    kubectl_manifest.istio
  ]
}
