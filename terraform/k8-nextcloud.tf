resource "kubernetes_namespace" "nextcloud-ns" {
  metadata {
    labels = {
      app             = "nextcloud"
      istio-injection = "enabled"
    }
    name = var.nextcloud_namespace
  }
}

data "kubectl_file_documents" "nextcloud-storage" {
  content = file("nextcloud-config/nextcloud-pvc.yaml")
}
resource "kubectl_manifest" "nextcloud-pvc" {
  count     = length(data.kubectl_file_documents.nextcloud-storage.documents)
  yaml_body = element(data.kubectl_file_documents.nextcloud-storage.documents, count.index)
  depends_on = [
    kubernetes_namespace.nextcloud-ns,
  ]
}
resource "helm_release" "nextcloud" {
  name       = "nextcloud"
  repository = var.nextcloud_repository
  chart      = "nextcloud"
  namespace  = var.nextcloud_namespace
  version    = var.nextcloud_version
  values = [
    "${file("nextcloud-config/nextcloud-config.yaml")}"
  ]
  depends_on = [
    kubernetes_namespace.nextcloud-ns,
    kubectl_manifest.calico,
    kubectl_manifest.nextcloud-pvc,
    kubectl_manifest.istio
  ]
}


data "kubectl_file_documents" "nextcloud-gateway" {
  content = file("nextcloud-config/nextcloud-gateway.yaml")
}
resource "kubectl_manifest" "nextcloud-gateway" {
  count     = length(data.kubectl_file_documents.nextcloud-gateway.documents)
  yaml_body = element(data.kubectl_file_documents.nextcloud-gateway.documents, count.index)
  depends_on = [
    kubectl_manifest.istio,
    helm_release.nextcloud,
  ]
}

data "kubectl_file_documents" "nextcloud-virtual-service" {
  content = file("nextcloud-config/nextcloud-virtualservice.yaml")
}

resource "kubectl_manifest" "nextcloud-virtual-service" {
  count     = length(data.kubectl_file_documents.nextcloud-virtual-service.documents)
  yaml_body = element(data.kubectl_file_documents.nextcloud-virtual-service.documents, count.index)
  depends_on = [
    kubectl_manifest.istio,
    helm_release.nextcloud,
  ]
}

data "kubectl_file_documents" "nextcloud-staging" {
  content = file("nextcloud-config/nextcloud-staging-cert.yaml")
}

resource "kubectl_manifest" "nextcloud-staging-cert" {
  count     = length(data.kubectl_file_documents.nextcloud-staging.documents)
  yaml_body = element(data.kubectl_file_documents.nextcloud-staging.documents, count.index)
  depends_on = [
    kubectl_manifest.istio,
    helm_release.nextcloud,
    helm_release.cert-manager,
    kubectl_manifest.letsencrypt-staging
  ]
}

data "kubectl_file_documents" "nextcloud-prod" {
  content = file("nextcloud-config/nextcloud-prod-cert.yaml")
}

resource "kubectl_manifest" "nextcloud-prod-cert" {
  count     = length(data.kubectl_file_documents.nextcloud-prod.documents)
  yaml_body = element(data.kubectl_file_documents.nextcloud-prod.documents, count.index)
  depends_on = [
    kubectl_manifest.istio,
    helm_release.nextcloud,
    helm_release.cert-manager,
    kubectl_manifest.letsencrypt-prod
  ]
}
