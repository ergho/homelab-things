variable "metallb_repository" {
  description = "Metallb helm repository"
  type        = string
  default     = "https://metallb.github.io/metallb"
}

variable "metallb_version" {
  description = "Metallb helm chart version"
  type        = string
  default     = "0.11.0"
}

variable "metallb_namespace" {
  description = "Metallb namespace"
  type        = string
  default     = "metallb-system"
}

variable "nextcloud_namespace" {
  description = "Nextcloud namespace"
  type        = string
  default     = "nextcloud"
}

variable "nextcloud_repository" {
  description = "Nextcloud helm chart repository"
  type        = string
  default     = "https://nextcloud.github.io/helm"
}

variable "nextcloud_version" {
  description = "Nextcloud helm chart version"
  type        = string
  default     = "2.12.1"
}

variable "cert-manager_namespace" {
  description = "Cert-manager Namespace"
  type        = string
  default     = "cert-manager"
}

variable "cert-manager_repository" {
  description = "Cert-manager helm repository"
  type        = string
  default     = "https://charts.jetstack.io"
}

variable "cert-manager_version" {
  description = "Cert-manager helm chart version"
  type        = string
  default     = "1.7.1"
}

//variable {
//  description = ""
//  type        = string
//  default     = ""
//}
