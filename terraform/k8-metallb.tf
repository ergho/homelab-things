resource "kubernetes_namespace" "metallb-system" {
  metadata {
    labels = {
      app = "metallb"
    }
    name = "metallb-system"
  }
}

resource "helm_release" "metallb" {
  name       = "metallb"
  repository = var.metallb_repository
  chart      = "metallb"
  namespace  = var.metallb_namespace
  version    = var.metallb_version

  values = [
    "${file("metallb-config/values.yaml")}"
  ]
  depends_on = [
    kubectl_manifest.calico,
    kubernetes_namespace.metallb-system
  ]
}
