# Storage Configuration Truenas + democratic-csi

Democratic-csi info here:
https://github.com/democratic-csi/democratic-csi

Truenas info here:
https://www.truenas.com/

## Helm repo add

```
helm repo add democratic-csi https://democratic-csi.github.io/charts/
helm repo update
helm repo search democratic-csi
```

## Installation of chart

Refer to the `truenas-nfs.yaml` file for configuration values

```
helm upgrade --install <release-name> \
    democratic-csi/democratic-csi \
    --namespace democratic-csi
    --create-namespace \
    --version "0.10.0" \
    --values ./truenas-nfs.yaml
```
